<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marca;
use Validator;

class MarcaController extends Controller
{
    public function index()
    {
        $marca = Marca::all();
        return response()->json([
            'success' => true,
            'data' => $marca
        ]);
    }

    public function show($id)
    {
        $marca = Marca::find($id);

        if (!$marca) {
            return response()->json([
                'success' => false,
                'message' => 'Marca con el ' . $id . ' no encontrada'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $marca->toArray()
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre_marca' => 'required|min:3',
            'activo' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ,'error'=>$validator->errors()]);
        }

        $marca = new Marca();
        $marca->nombre_marca = $request->nombre_marca;
        $marca->activo = $request->activo;

        if ($marca->save())
            return response()->json([
                'success' => true,
                'data' => $marca->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'La marca no pudo ser agregada'
            ], 500);
    }
    public function update(Request $request, $id)
    {
        $marca = Marca::find($id);

        if (!$marca) {
            return response()->json([
                'success' => false,
                'message' => 'Marca con el ' . $id . ' no encontrada'
            ], 400);
        }

        $updated = $marca->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'La marca no pudo ser actualizada'
            ], 500);
    }

    public function destroy($id)
    {
        //
    }
}
