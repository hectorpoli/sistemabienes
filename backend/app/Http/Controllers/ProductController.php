<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{
    public function index()
    {
        $products = auth()->user()->products;

        return response()->json([
            'success' => true,
            'data' => $products
        ]);
    }

    public function show($id)
    {
        $product = auth()->user()->products()->find($id);

        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'El producto con el id ' . $id . ' no se encontró'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $product->toArray()
        ], 400);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre_producto' => 'required|min:3',
            'activo' => 'required',
            'descripcion' => 'required',
            'marca_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ,'error'=>$validator->errors()]);
        }

        $product = new Product();
        $product->nombre_producto = $request->nombre_producto;
        $product->activo = $request->activo;
        $product->descripcion = $request->descripcion;
        $product->marca_id = $request->marca_id;

        if (auth()->user()->products()->save($product))
            return response()->json([
                'success' => true,
                'data' => $product->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'El producto no pudo ser agregado'
            ], 500);
    }

    public function update(Request $request, $id)
    {
        $product = auth()->user()->products()->find($id);

        if (!$product) {
            return response()->json([
                'success' => false,
                'message' => 'El producto con el ' . $id . ' no se encontró'
            ], 400);
        }

        $updated = $product->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'El producto no se pudo actualizar'
            ], 500);
    }

    public function destroy($id)
    {
        //
    }
}
