<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Compra;
use Validator;

class CompraController extends Controller
{
    public function index()
    {
        $compras = auth()->user()->compras;

        return response()->json([
            'success' => true,
            'data' => $compras
        ]);
    }

    public function show($id)
    {
        $compra = auth()->user()->compras()->find($id);

        if (!$compra) {
            return response()->json([
                'success' => false,
                'message' => 'La compra con el id ' . $id . ' no se encontró'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $compra->toArray()
        ], 400);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'precio' => 'required',
            'cantidad' => 'required|integer',
            'fecha_compra' => 'required|date',
            'proveedor_id' => 'required|integer',
            'producto_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ,'error'=>$validator->errors()]);
        }

        $compra = new Compra();
        $compra->precio = $request->precio;
        $compra->cantidad = $request->cantidad;
        $compra->fecha_compra = $request->fecha_compra;
        $compra->proveedor_id = $request->proveedor_id;
        $compra->producto_id = $request->producto_id;

        if (auth()->user()->compras()->save($compra))
            return response()->json([
                'success' => true,
                'data' => $compra->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'La compra no pudo ser agregada'
            ], 500);
    }

    public function update(Request $request, $id)
    {
        $compra = auth()->user()->compras()->find($id);

        if (!$compra) {
            return response()->json([
                'success' => false,
                'message' => 'La compra con el ' . $id . ' no se encontró'
            ], 400);
        }

        $updated = $compra->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'La compra no se pudo actualizar'
            ], 500);
    }

    public function destroy($id)
    {
        //
    }
}
