<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|min:3',
            'apellido' => 'required|min:3',
            'username' => 'required|min:3|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6'
            //'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ,'error'=>$validator->errors()]);
        }

        $user = User::create([
            'nombre' => $request->nombre,
            'apellido' => $request->apellido,
            'username' => $request->username,
            'rol' => 'User',
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $token = $user->createToken('TutsForWeb')->accessToken;

        return response()->json(['success' => true ,'token' => $token], 200);
    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('TutsForWeb')->accessToken;
            return response()->json(['success' => true ,'token' => $token, 'user' => auth()->user() ], 200);
        } else {
            return response()->json(['success' => false ,'error' => 'UnAuthorised'], 401);
        }
    }

    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }

    /**
     * Returns All User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(){
        $u = User::all();
        return response()->json([
            'success' => true,
            'data' => $u
        ]);
    }

    public function getById($id)
    {
        $u = User::find($id);

        if (!$u) {
            return response()->json([
                'success' => false,
                'message' => 'El usuario con el ' . $id . ' no fue encontrado'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $u->toArray()
        ], 200);
    }

    public function update(Request $request)
    {
        $u = User::find($request->id);

        if (!$u) {
            return response()->json([
                'success' => false,
                'message' => 'El usuario con el ' . $request->id . ' no fue encontrado'
            ], 400);
        }

        $updated = $u->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'El usuario no pudo ser actualizado'
            ], 500);
    }
}
