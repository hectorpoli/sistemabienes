<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proveedor;
use Validator;

class ProveedorController extends Controller
{
    public function index()
    {
        $p = Proveedor::all();
        return response()->json([
            'success' => true,
            'data' => $p
        ]);
    }

    public function show($id)
    {
        $p = Proveedor::find($id);

        if (!$p) {
            return response()->json([
                'success' => false,
                'message' => 'El proveedor con el ' . $id . ' no fue encontrado'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $p->toArray()
        ], 400);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre_proveedor' => 'required|min:3',
            'activo' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ,'error'=>$validator->errors()]);
        }

        $p = new Proveedor();
        $p->nombre_proveedor = $request->nombre_proveedor;
        $p->activo = $request->activo;

        if ($p->save())
            return response()->json([
                'success' => true,
                'data' => $p->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'El proveedor no pudo ser agregado'
            ], 500);
    }
    public function update(Request $request, $id)
    {
        $p = Proveedor::find($id);

        if (!$p) {
            return response()->json([
                'success' => false,
                'message' => 'El proveedor con el ' . $id . ' no fue encontrado'
            ], 400);
        }

        $updated = $p->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'El proveedor no pudo ser actualizado'
            ], 500);
    }

    public function destroy($id)
    {
        //
    }
}
