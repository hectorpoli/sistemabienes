<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventario;
use Validator;

class InventarioController extends Controller
{
    public function index()
    {
        $inv = auth()->user()->inventarios;

        return response()->json([
            'success' => true,
            'data' => $inv
        ]);
    }

    public function show($id)
    {
        $inv = auth()->user()->inventarios()->find($id);

        if (!$inv) {
            return response()->json([
                'success' => false,
                'message' => 'El inventario con el id ' . $id . ' no se encontró'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'data' => $inv->toArray()
        ], 400);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'existencia' => 'required|integer',
            'compra_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json([ 'success' => false ,'error'=>$validator->errors()]);
        }

        $inv = new Inventario();
        $inv->existencia = $request->existencia;
        $inv->compra_id = $request->compra_id;


        if (auth()->user()->inventarios()->save($inv))
            return response()->json([
                'success' => true,
                'data' => $inv->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'El producto no pudo ser agregado al inventario'
            ], 500);
    }

    public function update(Request $request, $id)
    {
        $inv = auth()->user()->inventarios()->find($id);

        if (!$inv) {
            return response()->json([
                'success' => false,
                'message' => 'El inventario con el ' . $id . ' no se encontró'
            ], 400);
        }

        $updated = $inv->fill($request->all())->save();

        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'El inventario no se pudo actualizar'
            ], 500);
    }

    public function destroy($id)
    {
        //
    }
}
