<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'productos';
    protected $fillable = [
        'nombre_producto', 'activo','descripcion'
    ];
}
