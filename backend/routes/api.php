<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'UserController@details');
    Route::get('user-id','UserController@getById');
    Route::post('user','UserController@update');
    Route::get('user-list','UserController@getAll');

    Route::resource('products', 'ProductController');
    Route::resource('marca', 'MarcaController');
    Route::resource('proveedor', 'ProveedorController');
    Route::resource('compra', 'CompraController');
    Route::resource('inventario', 'InventarioController');
});
