import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import {HomeComponent} from '@/home/home.component';
import { AuthGuard } from '@/guards/auth.guard';
import { Rol } from '@/modelos/rol';
import { RegisterComponent } from './register/register.component';
import { UsuariosListComponent } from './usuarios/usuarios-list/usuarios-list.component';
import { UsuariosUpdateComponent } from './usuarios/usuarios-update/usuarios-update.component';
import { MarcaComponent } from './marca/marca/marca.component';
import { MarcaNewComponent } from './marca/marca-new/marca-new.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent },
  { path: 'user', component: UsuariosListComponent},
  { path: 'user/edit', component: UsuariosUpdateComponent},
  { path: 'marcas', component: MarcaComponent},
  { path: 'marca/new' , component: MarcaNewComponent},
  { path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
