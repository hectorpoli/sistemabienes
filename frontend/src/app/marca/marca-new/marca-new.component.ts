import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutenticacionService } from '@/services/autenticacion.service';
import { AlertService } from '@/services/alert.service';
import { ApiService } from '@/services/api.service';
import { Marca } from '@/modelos/marca';

@Component({
  selector: 'app-marca-new',
  templateUrl: './marca-new.component.html',
  styleUrls: ['./marca-new.component.css']
})
export class MarcaNewComponent implements OnInit {

  marca: Marca;
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  estado = [{'id': '0', 'name': 'Inactivo'}, {'id': '1', 'name': 'Activo'}];

  constructor(
    private apiService: ApiService,
    private alertService: AlertService,
    private router: Router,
    private authenticationService: AutenticacionService,
    private formBuilder: FormBuilder
  ) {
    if (!this.authenticationService.currentUserValue) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      activo: [this.estado[1]['id'], [Validators.required]]
    });
  }

  get f() { return this.registerForm.controls; }

  registrar() {
    this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      this.loading = true;
      const dataRegistro = {
        'nombre_marca' : this.f.nombre.value,
        'activo' : this.f.activo.value
      };
      this.apiService.postData('marca', dataRegistro)
        .pipe(first())
        .subscribe(
          data => {
              if ( data['success'] === true) {
                this.alertService.success('La marca fue registra correctamente', true);
                this.router.navigate(['/marcas']);
              } else {
                this.alertService.error(data['error']);
                this.loading = false;
              }
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          }
        );
  }

}
