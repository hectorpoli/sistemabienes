import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AutenticacionService } from '@/services/autenticacion.service';
import { AlertService } from '@/services/alert.service';
import { ApiService } from '@/services/api.service';
import { Marca } from '@/modelos/marca';

@Component({
  selector: 'app-marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.css']
})
export class MarcaComponent implements OnInit {

  marcas: Marca[];

  constructor(
    private apiService: ApiService,
    private alertService: AlertService,
    private router: Router,
    private authenticationService: AutenticacionService
  ) {
    if (!this.authenticationService.currentUserValue) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    this.apiService.getData('marca')
    .pipe(first())
    .subscribe(
      data => {
          if ( data['success'] === true) {
            this.marcas = data['data'];
          } else {
            this.alertService.error(data['error']);
          }
      },
      err => {
        this.alertService.error(err);
      }
    );
  }

  editar(id) {
    localStorage.setItem('marca-edit', JSON.stringify(this.marcas[id]));
    // this.router.navigate(['/user/edit']);
  }

  agregar() {
    this.router.navigate(['/marca/new']);
  }

}
