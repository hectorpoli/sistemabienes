import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AutenticacionService } from '@/services/autenticacion.service';
import { AlertService } from '@/services/alert.service';
import { ApiService } from '@/services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;

  password: any;
  nombre: any;
  email: any;
  apellido: any;
  username: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AutenticacionService,
    private apiService: ApiService,
    private alertService: AlertService
  ) {
    // redirect to home if already logged in
    /*if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }*/
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      apellido: ['', [Validators.required, Validators.minLength(3)]],
      username: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.registerForm.controls; }

  registrar() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      this.loading = true;
      const dataRegistro = {
        'password' : this.f.password.value,
        'email' : this.f.email.value,
        'nombre' : this.f.nombre.value,
        'apellido' : this.f.apellido.value,
        'username' : this.f.username.value
      };
      this.apiService.postData('register', dataRegistro)
          .pipe(first())
          .subscribe(
              data => {
                  if ( data['success'] === true) {
                    this.alertService.success('Usuario registrado correctamente', true);
                    this.router.navigate(['/login']);
                  } else {
                    this.alertService.error(data['error']);
                    this.loading = false;
                  }
              },
              error => {
                this.alertService.error(error);
                this.loading = false;
              });
  }

}
