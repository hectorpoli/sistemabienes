import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {AutenticacionService} from '@/services/autenticacion.service';
import {Usuario} from '@/modelos/usuario.modelo';
import { Rol } from '@/modelos/rol';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  currentUser: Usuario;

  constructor(
    private router: Router,
    private authenticationService: AutenticacionService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.rol === Rol.Admin;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  ngOnInit() {}

}
