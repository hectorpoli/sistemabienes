import { Component, OnInit } from '@angular/core';

import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AutenticacionService } from '@/services/autenticacion.service';
import { AlertService } from '@/services/alert.service';
import { ApiService } from '@/services/api.service';
import { Usuario } from '@/modelos/usuario.modelo';

@Component({
  selector: 'app-usuarios-list',
  templateUrl: './usuarios-list.component.html',
  styleUrls: ['./usuarios-list.component.css']
})
export class UsuariosListComponent implements OnInit {

  usuarios: Usuario[];

  constructor(
    private apiService: ApiService,
    private alertService: AlertService,
    private router: Router,
    private authenticationService: AutenticacionService
  ) {
    if (!this.authenticationService.currentUserValue) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    this.apiService.getData('user-list')
    .pipe(first())
    .subscribe(
      data => {
          if ( data['success'] === true) {
            this.usuarios = data['data'];
          } else {
            this.alertService.error(data['error']);
          }
      },
      err => {
        this.alertService.error(err);
      }
    );
  }

  editar(id) {
    localStorage.setItem('user-edit', JSON.stringify(this.usuarios[id]));
    this.router.navigate(['/user/edit']);
  }
}
