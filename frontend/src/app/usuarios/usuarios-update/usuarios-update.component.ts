import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutenticacionService } from '@/services/autenticacion.service';
import { AlertService } from '@/services/alert.service';
import { ApiService } from '@/services/api.service';
import { Usuario } from '@/modelos/usuario.modelo';

@Component({
  selector: 'app-usuarios-update',
  templateUrl: './usuarios-update.component.html',
  styleUrls: ['./usuarios-update.component.css']
})
export class UsuariosUpdateComponent implements OnInit {

  editForm: FormGroup;
  loading = false;
  submitted = false;

  usuario: Usuario;

  password: any;
  nombre: any;
  email: any;
  apellido: any;
  username: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AutenticacionService,
    private userService: ApiService,
    private alertService: AlertService
  ) {
    if (!this.authenticationService.currentUserValue) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem('user-edit'));
    this.editForm = this.formBuilder.group({
      nombre: [this.usuario['nombre'], [Validators.required, Validators.minLength(3)]],
      apellido: [this.usuario['apellido'], [Validators.required, Validators.minLength(3)]],
      username: [this.usuario['username'], [Validators.required, Validators.minLength(3)]],
      email: [this.usuario['email'], [Validators.required, Validators.email]]
    });
  }

  get f() { return this.editForm.controls; }

  actualizar() {
    this.submitted = true;

      // stop here if form is invalid
      if (this.editForm.invalid) {
          return;
      }

      this.loading = true;
      const dataRegistro = {
        'id' : this.usuario['id'],
        'email' : this.f.email.value,
        'nombre' : this.f.nombre.value,
        'apellido' : this.f.apellido.value,
        'username' : this.f.username.value
      };

      this.userService.postData('user', dataRegistro)
      .pipe(first())
      .subscribe(
        data => {
            if ( data['success'] === true) {
              this.alertService.success('Usuario actualizado correctamente', true);
              this.router.navigate(['/user']);
            } else {
              this.alertService.error(data['error']);
              this.loading = false;
            }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }

}
