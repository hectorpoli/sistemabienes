export interface Usuario {
  id: Number;
  nombre: String;
  apellido: String;
  email: String;
  password: String;
  rol: String;
  username: String;
}
