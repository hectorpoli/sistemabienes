import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { Usuario } from '@/modelos/usuario.modelo';
import { AutenticacionService } from '@/services/autenticacion.service';
import { ApiService } from '@/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  currentUser: Usuario;
  userFromApi: Usuario;

  constructor(
    private userService: ApiService,
    private authenticationService: AutenticacionService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
   }

  ngOnInit() {}

}
