import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { AutenticacionService } from '@/services/autenticacion.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = 'http://localhost:8000/api/';
  token: any;

  constructor(
    private http: HttpClient, private autenticacionService: AutenticacionService) {}

  postData(url, data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
    return this.http.post(this.baseUrl + url + '/', data, httpOptions);
  }

  getData(url) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
    return this.http.get(this.baseUrl + url, httpOptions);
  }

  getToken(token) {
    this.token = token;
  }

  sendToken() {
    return this.token;
  }

}
